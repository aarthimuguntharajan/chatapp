const routes = require('./src/routes')
const query = require('./src/queries')
const {addUserSocket,removeUserSocket} = require('./src/socket')
require('./models/index')
const app = require('./src/routes');
const cors = require('cors')
app.use(cors())
const http = require('http').createServer(app);
const io = require('socket.io')(http, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"]
  }
});
const dotenv = require('dotenv')
const path = require('path')
dotenv.config({path: path.resolve(__dirname, '../.env')})

app.get('/', (req, res) => {
  res.send('<h1>Welcome to chat App</h1>');
});

const port = process.env.PORT || 5555

//const http = require('http').Server(app)

app.set('socketIo',io);

//To listen to messages
io.on('connection', (socket)=>{
  console.log('user connected')
  console.log(socket.handshake.query.userId,socket.id)
  if (socket.handshake.query.userId !== undefined){
    addUserSocket(socket.handshake.query.userId,socket.id)
  }

  /* Get the user's recent Chat list	*/
  socket.on(`recentChat`, async (data) => {
    console.log("Inside recent chat server side")
    if (data.userId == '') {
        this.io.emit(`recentChat-response`, {
        error : true,
        message : "USER_NOT_FOUND"
       });
     }else{
       try {
         const [UserInfoResponse, chatlistResponse] = await Promise.all([
           query.getUserById(data.userId),
           query.recentChats(data.userId)
           ]);
          io.to(socket.id).emit(`recentChat-response`, {
            error : false,
            userId: data.userId,
            recentChats : chatlistResponse,
            userInfo : UserInfoResponse
          });
          socket.broadcast.emit(`recentChat-response`,{
            error : false,
            userId: data.userId,
            recentChats : chatlistResponse,
            userInfo : UserInfoResponse
          });
      } catch ( error ) {
        io.to(socket.id).emit(`recentChat-response`,{
          error : true ,
          chatList : []
        });
      }
    }
  });

  socket.on('typing', (data)=>{
    if(data.isTyping === true)
      socket.broadcast.emit('display', data)
    else
      socket.broadcast.emit('display', data)
  })

  socket.on('disconnect', ()=>{
    console.log('Disconnected')
    removeUserSocket(socket.handshake.query.userId,socket.id)
  })
});

http.listen(port, () => {
  console.log('Chat App is listening on  http://localhost:' + port)
})

module.exports = app