const jwt = require('jsonwebtoken')



function verifyToken (req, res, next) {
  const token = req.headers['x-access-token']

  if (!token) {
    return res.status(403).send({
      auth: false,
      token: null,
      message: 'No token provided.'
    })
  }

  jwt.verify(token, process.env.jwtSecret, function (err, decoded) {
    if (err && err.name === 'TokenExpiredError') {
      return res.status(200).send({
        auth: true,
        token: 'expired',
        message: 'Token Expired'
      })
    } else if (err) {
      return res.status(500).send({
        auth: false,
        token: null,
        message: 'Failed to authenticate token.'
      })
    }

    req.token = {
      id: decoded.id,
      email: decoded.email
    }

    next()
  })
}

module.exports = verifyToken
