const express = require('express')
const chat = express()
const mongoose = require('mongoose')
const query = require('../queries')
const {getUserTimestamp, updateUserTimestamp} = require('../socket')
const { check, validationResult } = require('express-validator')
const jwtToken = require('../../middleware/jwtToken')
chat.use(express.json())

//This API is used to update the chat by adding new message in a chat,
//editing a previous message in a chat and 
//deleting a message from a chat of any user.
chat.post('/update',
check('action', 'action is required').isIn(['NEW','EDIT','DELETE']),
jwtToken,
 async(request,response) => {
    const io = request.app.get('socketIo');
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({ errors: errors.array() })
    }
    const head = new Date(request.headers['last-time-stamp'])
    const sockettime = new Date( await getUserTimestamp(request.body.senderId))
    if ( head.getTime() !== sockettime.getTime() ) {
        const valueSet = await query.getLagdetails(request.body.senderId,request.body.receiverId,head)
        io.sockets.emit('upto-date', {
            senderId: request.body.senderId,
            receiverId: request.body.receiverId,
            lastTimeStamp: await getUserTimestamp(request.body.senderId),
            data: valueSet
        });
    }
    else if(request.body.action === "NEW"){  
        if(!request.body.senderId || !request.body.receiverId){
            return response.status(400).json({ msg : 'Bad Payload' })
        }  
        const senderCheck = mongoose.isValidObjectId(request.body.senderId)
        const receiverCheck = mongoose.isValidObjectId(request.body.receiverId)
        if (!(senderCheck || receiverCheck)) {
            return response.status(400).json({ msg : 'Not a valid userId' })
        }  
        const saved = await query.sendNewMessage(request.body);
        if(!saved._id){
            response.status(500).send({
                msg : "Message not saved"
            })
        }
        else{
            io.sockets.emit('message', {
                action : "NEW",
                lastTimeStamp : saved.updatedAt,
                data: saved
            })
            await updateUserTimestamp(request.body.senderId,saved.updatedAt)
            return response.status(200).send({
                        msg : "Message sent",
                        auth : true,
                        lastTimeStamp : saved.updatedAt,
                        action : "NEW",
                        data: saved
                    })
            
        }        
    } else if (request.body.action === "EDIT"){
        if(!request.body.message || !request.body.messageId){
            return response.status(400).json({ msg : 'Bad Payload' })
        }
        const messageCheck = mongoose.isValidObjectId(request.body.messageId)
        if(!messageCheck){
            return response.status(400).json({ msg : 'Invalid messageId' })
        }
        try {
            const edited = await query.editMessage(request.body.messageId,request.body.message)
            io.sockets.emit('message', {
                action : "EDIT",
                lastTimeStamp : edited.updatedAt,
                data: edited
            });
            await updateUserTimestamp(request.body.senderId,edited.updatedAt)
            return response.status(200).send({
                msg : "Message edited",
                auth : true,
                lastTimeStamp : edited.updatedAt,
                action : "EDIT",
                data: edited
            })
        }catch(err){
            response.status(500).send({
                msg : "Message not edited"
            })
        }
    } else if (request.body.action === "DELETE"){
        if(!request.body.messageId){
            return response.status(400).json({ msg : 'Bad Payload' })
        }
        const messageCheck = mongoose.isValidObjectId(request.body.messageId)
        if(!messageCheck){
            return response.status(400).json({ msg : 'Invalid messageId' })
        }
        try {
            const deleted = await query.deleteMessage(request.body.messageId)
            io.sockets.emit('message', {
                action : "DELETE",
                lastTimeStamp : deleted.updatedAt,
                data: deleted
            });
            await updateUserTimestamp(request.body.senderId,deleted.updatedAt)
            return response.status(200).send({
                msg : "Message deleted",
                auth : true,
                lastTimeStamp : deleted.updatedAt,
                action : "DELETE",
                data: deleted
            })
        }catch(err){
            response.status(500).send({
                msg : "Message not deleted"
            })
        }

    }
    
})

/* This API is used to get list of previous messages in a chat, 
* new messages in a chats and count of new messages in a chat with particular user.
*/

chat.post('/activeChat',
    check('friendId', 'friendId is required').notEmpty(),
    check('userId', 'userId is required').notEmpty(),
    jwtToken,
    async(req,res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() })
        }
        const friendCheck = mongoose.isValidObjectId(req.body.friendId)
        const userCheck = mongoose.isValidObjectId(req.body.userId)
        if (!(friendCheck || userCheck)) {
            return res.status(400).json({ msg : 'Not a valid userId' })
        }

        try {
            const chat = await query.getChat(req.body.friendId,req.body.userId)
            const friendDetails = await query.getUserById(req.body.friendId)
            const lastTimeStamp = getUserTimestamp(req.body.userId)
            const lastSeen = getUserTimestamp(req.body.friendId)
            const unReadCount = chat.filter(item => {
                return ((item.senderId.equals(friendDetails.id)) && !(item.seenAt)) ? true : false
            }).length
            res.status(200).send({
                auth : true,
                lastTimestamp : lastTimeStamp,
                data : {
                    friendId: friendDetails.id,
                    friendName : friendDetails.firstName,
                    profilePic : friendDetails.profilePic,
                    friendStatus: friendDetails.isOnline ? 'Online' : 'Offline',
                    lastSeenAt: lastSeen,
                    unreadCount: unReadCount,
                    previousMessages: chat
                }
            })


        } catch (err) {
            return res.status(500).send({
                msg:'Internal Server error'
            })
        }

})

/* 
* This api is to update the delivered and seen time 
* for messages
*/

chat.post('/updateMessageInfo',
    check('messageIds', 'Atleast one MessageId is required').notEmpty(),
    check('isDelivered', 'isDelivered is required').notEmpty(),
    check('isSeen', 'isSeen is required').notEmpty(),
    jwtToken,
    async(req,res) => {
        const io = req.app.get('socketIo');
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() })
        }
        const {messageIds,isDelivered,isSeen} = req.body
        try {
            const updated = await query.updateMessageInfo(messageIds,isDelivered,isSeen)
            
            res.status(200).send({
                auth : true,
                messageIds: messageIds,
                deliveredAt: Date.now(),
                seenAt: Date.now()
            })

            if(isDelivered && isSeen){
                io.sockets.emit('messageInfo', {
                    messageIds: messageIds,
                    deliveredAt: Date.now(),
                    seenAt: Date.now()
                });
            } else if(isDelivered){
                io.sockets.emit('messageInfo', {
                    messageIds: messageIds,
                    deliveredAt: Date.now()
                });
            }else if(isSeen){
                io.sockets.emit('messageInfo', {
                    messageIds: messageIds,
                    seenAt: Date.now()
                });
            }

        } catch (err) {
            return res.status(500).send({
                msg:'Internal Server error'
            })
        }

})

chat.post('/updateDeliveredAt',
    check('userId', 'userId is required').notEmpty(),
    check('friendIds', 'Atleast one friendId is required').notEmpty(),
    jwtToken,
    async(req,res) => {
        const io = req.app.get('socketIo');
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() })
        }
        const {userId,friendIds} = req.body
        try {
            const messages = await query.updateDeliveredAt(userId,friendIds)
            const messageIds = (messages.upserted).map((item)=>item._id)
            console.log(messages,messageIds)
            io.sockets.emit('messageInfo', {
                messageIds: messageIds,
                deliveredAt: Date.now()
            });
            res.status(200).send({
                auth : true,
                msg : "Delivered Time updated"
            })

        } catch (err) {
            return res.status(500).send({
                msg:'Internal Server error' + err
            })
        }

})

module.exports = chat 