const query = require('./queries')
var userSocket = []

async function addUserSocket(userId,socketId) {
    if(userId){
        const index = userSocket.findIndex(user => user.userId === userId);
        if (index !== -1){
            userSocket[index].socketIds.push(socketId)
        } else {
            const user = {
                userId : userId,
                socketIds : [socketId],
                lastTimeStamp : await query.getLasttimestamp(userId)
            }
            userSocket.push(user)
        }
        console.log(userSocket)
        await query.makeUserOnline(userId)

    }
}

async function removeUserSocket(userId,socketId){
    if(userId){
        const index = userSocket.findIndex(user => user.userId === userId);
        userSocket[index].socketIds = userSocket[index].socketIds.filter(function(sockets) {
            return sockets !== socketId
        })
        if(userSocket[index].socketIds.length === 0){
            await query.makeUserOffline(userId)
        }
    }
}

function getUserTimestamp(userId){
    const index = userSocket.findIndex(user => user.userId === userId)
    if(index !== -1){
        return userSocket[index].lastTimeStamp 
    } else {
        return null
    }
    
}

function updateUserTimestamp(userId,lastTimeStamp){
    const index = userSocket.findIndex(user => user.userId === userId)
    if(index !== -1){
        userSocket[index].lastTimeStamp  = lastTimeStamp
    } else {
        return null
    }
    
}

module.exports = {addUserSocket, removeUserSocket,getUserTimestamp, updateUserTimestamp}
