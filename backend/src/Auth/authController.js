const express = require('express')
const auth = express()
const { check, validationResult } = require('express-validator')
const query = require('../queries')
const jwt = require('jsonwebtoken')

// @route POST /auth/register
// @desc     Register New users
// @access   Public
auth.post('/register',
  check('firstName', 'First Name is required').notEmpty(),
  check('lastName', 'Last Name is required').notEmpty(),
  check('email', 'Please include a valid email').isEmail(),
  check(
   'password',
   'Please enter a password with 8 or more characters'
  ).isLength({ min: 8 }),
  check('phone', 'Phone is required').notEmpty(), async (request, response) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).send(errors.array()[0])
    }
    try{
		let user = await query.checkUniqueEmailPhone(request.email, request.phone)
        if (user.length !== 0) {
            return response
            .status(400)
            .send({ msg: 'User already exists' });
		}
		const registration = await query.registerNewUser(request.body) 
        if (!registration._id){
            return response.status(404).send({
                msg: 'User Details not registered, try again'
            })
        }
        else{
            return response.status(200).send({
                msg: registration.firstName + ' '+ registration.lastName + ' your details registered successfully'
            })
        }
    }   catch (err) {
            return response.status(500).send({
                msg:'Internal Server error'
            });
        }
    
})

//API to user login. With email finds the 
//row in db and changes isOnline to true
auth.post('/login', 
		check('email', 'Please include a valid email').isEmail(),
		check('password','Please enter a password with 8 or more characters').isLength({ min: 8 }),
        async (request, response) => {
		const errors = validationResult(request);
		if (!errors.isEmpty()) {
			return response.status(400).send( errors.array()[0]);
		  }
		const data = {
			email : (request.body.email).toLowerCase(),
			password : request.body.password
		}
		try {
			const result = await query.getUserByEmail(data.email);
			if(result ===  null || result === undefined) {
				response.status(500).send({
					auth : false,
					msg : "INCORRECT EMAIL ! USER_LOGIN_FAILED"
				});
			}
			else {
				if(  data.password ===  result.password ) {
					const changedToOnline = await query.makeUserOnline(result._id);
					if(changedToOnline.isOnline === true){
						const token = jwt.sign({
							id: changedToOnline._id,
							email: changedToOnline.email
						  }, process.env.jwtSecret, {
							expiresIn: 86400000 * 15 // 15 days
						  })
						response.status(200).json({
							auth : true,
							token : token,
							data : {
								loggedData : {
									userId : changedToOnline._id,
									firstName : changedToOnline.firstName,
									lastName : changedToOnline.lastName,
									email : changedToOnline.email,
									profilePic : changedToOnline.profilePic
								}
							},
							msg : "USER_LOGIN_OK"
						});
					}else{
						response.status(500).json({
							auth : false,
							msg : "USER NOT CHANGED TO ONLINE! USER_LOGIN_FAILED"
						});
					}
					
				} else {
					response.status(500).send({
						auth : false,
						msg : "INCORRECT PASSWORD! USER_LOGIN_FAILED"
					});
				}					
			}
		}
		catch (error) {
			response.status(404).send({
				auth : false,
				msg : "USER_LOGIN_FAILED"
			});
		}				
})


module.exports = auth
