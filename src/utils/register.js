import * as axios from 'axios'

class Register {

    registerUser(data){
       return new Promise( async (resolve,reject) => {
           try{
            const response = await axios.post('http://localhost:5555/auth/register', {
                method: "POST",
                headers: {
                    'content-type':'application/json'
                },
                body: JSON.stringify(data)
            }).then(response => 
                response.json())
            resolve(response.msg);
           }catch(error){
               reject(error)
           }
       })
    }
}
export default new Register()