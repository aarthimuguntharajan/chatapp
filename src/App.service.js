import * as axios from 'axios'
const services = {
    postAPI: (url, payload) => {
        const token = localStorage.getItem('token');
        let httpOptions;
        if (token !== null) {
            httpOptions = {
                headers: {
                'Content-Type': 'application/json',
                'x-access-token': token,
                'last-time-stamp' : localStorage.getItem('lastTimeStamp')
                }
            };
        } else {
            httpOptions = {
                headers: {
                'Content-Type': 'application/json'
                }
            };
        }
        return new Promise( async (resolve,reject) => {
            try{
                const logDetails = await axios.post(url, payload, httpOptions)
                resolve(logDetails.data)
            }catch(error){
                if(error.response){
                    resolve(error.response.data) 
                } else {
                    reject(error)
                }
                
            }
        }) 
    }
}

export default services;