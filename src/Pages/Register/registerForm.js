import React, { Component } from 'react'
import SweetAlert from 'react-bootstrap-sweetalert';
import FileBase64 from 'react-file-base64';


class register extends Component {
    constructor(props){
        super(props);
        this.state = { 
            password: '',
            email: '',
            phone: '',
            firstName: '',
            lastName: '',
            profilePic: [],
            alert : null   
        };
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    hideAlert() {
        this.setState({
          alert: null
        });
    }
    async handleSubmit(e){
        e.preventDefault();
        const data = {
            email: this.state.email,
            password : this.state.password,
            firstName : this.state.firstName,
            lastName : this.state.lastName,
            phone : this.state.phone,
            profilePic: this.state.profilePic
        }
        
        await fetch('http://localhost:5555/auth/register', {
            method : 'POST' , 
            headers: {
                'content-type':'application/json'
            },        
            body : JSON.stringify(data)
        })
        .then(res => res.json())
        .then((data) => {
            if (data.status === 200){
                const getAlert = () => (
                    <SweetAlert 
                        success
                        title= "Message"
                        onConfirm={() => {this.hideAlert();window.location.href = '/login'}}
                    >
                        {data.msg}
                    </SweetAlert>
                );
                        
                this.setState({
                alert: getAlert()
                })
            } else {
                const getAlert = () => (
                    <SweetAlert 
                        success 
                        title= "Message"
                        onConfirm={() => this.hideAlert()}
                    >
                        {data.msg}
                    </SweetAlert>
                    );
                
                    this.setState({
                    alert: getAlert()
                    })
            }        
        })              
        
    }; 
    getFiles(files){
        this.setState({ profilePic: files.base64 })
    }
    render() {
        return (
            <section class="testimonial py-5" id="testimonial" style={{marginTop:'80px'}}>
                <div class="container">
                    <div class="row ">
                        <div class="col-md-4 py-5 bg-primary text-white text-center ">
                            <div class=" ">
                                <div class="card-body">
                                    <img src="http://www.ansonika.com/mavia/img/registration_bg.svg" style={{width:'30%'}} alt=""/>
                                    <h2 class="py-3">Registration</h2>
                                    <p>This application is to communicate with your loved ones..<stong>Your data won't be misused..Don't worry :)</stong>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 py-5 border">
                            <h4 class="pb-4">Please fill with your details</h4>
                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <input id="firstName" name="firstName" placeholder="First Name" class="form-control" type="text" required onChange={(event) => this.setState({firstName: event.target.value})} />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input id="lastName" name="lastName" placeholder="Last Name" class="form-control" type="text" required onChange={(event) => this.setState({lastName: event.target.value})} />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input id="phone" name="phone" placeholder="Mobile No." class="form-control" required type="text" onChange={(event) => this.setState({phone: event.target.value})}  />                        </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required onChange={(event) => this.setState({email: event.target.value})} />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required onChange={(event) => this.setState({password: event.target.value})} />
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Choose Profile image</label>
                                        <div class="form-group col-md-6">
                                            <FileBase64 id="profilePic" name="profilePic" required onDone={this.getFiles.bind(this) } />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-danger" onClick = { this.handleSubmit }>Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {this.state.alert}
            </section>
        )
    }
}

export default register;
